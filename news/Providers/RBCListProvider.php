<?php

namespace News\Providers;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class RBCListProvider implements NewsListProvider
{
    private $url = 'https://www.rbc.ru/';
    private $client;
    private $crawler;

    public function __construct()
    {
        $this->client = new Client(['timeout'  => 60.0]);
        $this->crawler = new Crawler();
    }

    public function list(): array
    {
        $html = $this->client->get($this->url)->getBody()->getContents();
        $this->crawler->add($html);

        return $this->crawler->filter('a.js-news-feed-item')->each(function(Crawler $a, $i) {
            return $a->attr('href');
        });
    }
}
