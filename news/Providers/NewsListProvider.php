<?php

namespace News\Providers;

interface NewsListProvider
{
    const RBC_PROVIDER = 'rbc';

    public function list(): array;
}
