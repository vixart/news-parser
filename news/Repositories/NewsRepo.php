<?php

namespace News\Repositories;

use News\Entities\News;
use News\Parsers\NewsParser;

interface NewsRepo
{
    public function saveFromParser(NewsParser $news): News;

    public function getArticles(int $limit);

    public function getById(int $id);
}
