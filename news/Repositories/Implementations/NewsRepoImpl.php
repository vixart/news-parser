<?php

namespace News\Repositories\Implementations;

use App\Models\News as NewsModel;
use App\Models\NewsProvider as NewsProviderModel;
use News\Entities\News;
use News\Entities\NewsProvider;
use News\Parsers\NewsParser;
use News\Repositories\NewsRepo;

class NewsRepoImpl implements NewsRepo
{
    public function saveFromParser(NewsParser $parser): News
    {
        /** @var NewsProvider $provider */
        $provider = NewsProviderModel::firstWhere('name', $parser->getProviderName());
        return NewsModel::firstOrCreate(
            ['provider_id' => $provider->getId(), 'hash' => $parser->getHash()],
            ['title' => $parser->getTitle(), 'text' => $parser->getText(), 'image' => $parser->getImage()]
        );
    }

    public function getArticles(int $limit)
    {
        return NewsModel::orderBy('created_at', 'desc')->limit($limit)->get();
    }

    public function getById(int $id): News
    {
        return NewsModel::where('id', '=', $id)->firstOrFail();
    }
}
