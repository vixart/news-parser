<?php

namespace News\Parsers;

interface NewsParser
{
    public function getProviderName(): string;

    public function getHash(): string;

    public function getTitle(): string;

    public function getText(): string;

    public function getImage(): ?string;
}
