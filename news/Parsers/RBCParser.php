<?php

namespace News\Parsers;

use GuzzleHttp\Client;
use News\Providers\NewsListProvider;
use Symfony\Component\DomCrawler\Crawler;

class RBCParser implements NewsParser
{
    private $crawler;
    private $client;
    private $providerName;

    public function __construct(string $url)
    {
        $this->providerName = NewsListProvider::RBC_PROVIDER;
        $this->client = new Client();
        $html = $this->client->get($url)->getBody()->getContents();
        $this->crawler = new Crawler($html);
    }

    public function getProviderName(): string
    {
        return $this->providerName;
    }

    public function getHash(): string
    {
        return $this->crawler->filter('.js-rbcslider-slide')->attr('data-id');
    }

    public function getTitle(): string
    {
        return $this->crawler->filter('[itemprop="headline"]')->first()->text();
    }

    public function getText(): string
    {
        return implode('', $this->crawler->filter('[itemprop="articleBody"]')->filter('p')->each(function($p, $i) {
            return $p->text();
        }));
    }

    public function getImage(): ?string
    {
        try {
            return $this->crawler->filter('.article__main-image__image')->first()->attr('src');
        } catch (\Exception $e) {
            return null;
        }
    }
}
