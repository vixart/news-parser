<?php

namespace News\Entities;

interface News
{
    public function getId(): int;

    public function getProviderId(): int;

    public function setProviderId(int $providerId): void;

    public function getHash(): string;

    public function setHash(string $hash): void;

    public function getTitle(): string;

    public function setTitle(string $title): void;

    public function getText(): string;

    public function setText(string $text): void;

    public function getImage(): ?string;

    public function setImage(?string $image): void;
}
