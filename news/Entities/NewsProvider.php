<?php

namespace News\Entities;

interface NewsProvider
{
    public function getId(): int;

    public function getName(): string;

    public function setName(string $name): void;
}
