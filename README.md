### Развертывание проекта

- Клонируем проект git clone git@gitlab.com:vixart/news-parser.git project
- composer install
- npm install && npm run dev
- изменить подключение к базе данных в .env файле
- установить QUEUE_CONNECTION=redis в .env и config/queue.php файлах
- php artisan serve
- redis-server
- php artisan queue:work redis --tries=5
- php artisan rbc_news:fetch
- ##### возможна работа без redis, установить QUEUE_CONNECTION=sync, при таком подходе программа будет падать из-за неудачного получения данных по url.
