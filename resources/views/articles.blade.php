<html>
<head>
    <title>Статьи</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 text-center">
                <h2><strong>Последние новости</strong></h2>
            </div>
        </div>
    </div>
    <div id="app">
        @foreach ($newsList as $news)
            <article-preview-component
                :id={{ $news->id }}
                title="{{ $news->title }}"
                text="{{ $news->text }}">
            </article-preview-component><br>
        @endforeach
    </div>
    <script src="{{ asset('js/app.js')}}"></script>
</body>
</html>
