<html>
<head>
    <title>{{ $news->title }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div id="app">
        <article-component
            :id={{ $news->id }}
            title="{{ $news->title }}"
            text="{{ $news->text }}"
            image="{{ $news->image }}">
        </article-component>
    </div>
    <script src="{{ asset('js/app.js')}}"></script>
</body>
</html>
