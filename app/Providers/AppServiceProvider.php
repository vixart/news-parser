<?php

namespace App\Providers;

use App\Models\News as NewsModel;
use Illuminate\Support\ServiceProvider;
use News\Entities\News;
use News\Repositories\Implementations\NewsRepoImpl;
use News\Repositories\NewsRepo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(News::class, NewsModel::class);
        $this->app->singleton(NewsRepo::class, NewsRepoImpl::class);
    }
}
