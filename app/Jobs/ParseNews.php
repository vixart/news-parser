<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use News\Parsers\NewsParser;
use News\Repositories\NewsRepo;

class ParseNews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parser;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @param string $parser
     * @param string $url
     */
    public function __construct(string $parser, string $url)
    {
        $this->parser = $parser;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @param NewsRepo $newsRepo
     * @return void
     * @throws BindingResolutionException
     */
    public function handle(NewsRepo $newsRepo)
    {
        /** @var NewsParser $parser */
        $parser = app()->make($this->parser, ['url' => $this->url]);
        $newsRepo->saveFromParser($parser);
        info($parser->getTitle());
    }
}
