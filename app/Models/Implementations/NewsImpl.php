<?php

namespace App\Models\Implementations;

trait NewsImpl
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getProviderId(): int
    {
        return $this->provider_id;
    }

    public function setProviderId(int $providerId): void
    {
        $this->provider_id = $providerId;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): void
    {
        $this->image = $image;
    }
}
