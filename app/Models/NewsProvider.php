<?php

namespace App\Models;

use App\Models\Implementations\NewsProviderImpl;
use Illuminate\Database\Eloquent\Model;
use News\Entities\NewsProvider as INewsProvider;

class NewsProvider extends Model implements INewsProvider
{
    use NewsProviderImpl;

    protected $table = 'news_providers';
    protected $guarded = ['id'];
}
