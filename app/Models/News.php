<?php

namespace App\Models;

use App\Models\Implementations\NewsImpl;
use Illuminate\Database\Eloquent\Model;
use News\Entities\News as INews;

class News extends Model implements INews
{
    use NewsImpl;

    protected $table = 'news';
    protected $guarded = ['id'];
}
