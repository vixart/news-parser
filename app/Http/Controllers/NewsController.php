<?php

namespace App\Http\Controllers;

use News\Repositories\NewsRepo;

class NewsController extends Controller
{
    public function articles(NewsRepo $repo)
    {
        $news = $repo->getArticles(15);
        return view('articles', ['newsList' => $news]);
    }

    public function article(int $id, NewsRepo $repo)
    {
        $news = $repo->getById($id);
        return view('article', ['news' => $news]);
    }
}
