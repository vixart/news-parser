<?php

namespace App\Console\Commands;

use App\Jobs\ParseNews;
use Illuminate\Console\Command;
use News\Parsers\RBCParser;
use News\Providers\RBCListProvider;

class FetchRBCNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbc_news:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news from rbc.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $provider = new RBCListProvider();
        $newsList = $provider->list();

        foreach ($newsList as $url) {
            ParseNews::dispatch(RBCParser::class, $url);
        }

        echo "Jobs pushed to queue: ".count($newsList)."\n";
        return 0;
    }
}
